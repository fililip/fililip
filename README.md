# Welcome.

GitLab seems to have implemented this feature at last, so here I am, writing this fancy page.

Just a regular dude who enjoys making stuff when he feels like it.

I'm familiar with Rust, JavaScript, TypeScript, C#, Python, and a bit of C++.

Some of my projects include (but are not limited to):

- [Nitrosubs](https://nitrosubs.live), a former VOD streaming service which was my first large web project, which now serves as my about page
- [bad-visualizer-rs](https://gitlab.com/fililip/bad-visualizer-rs), an audio player made using [FMOD](https://fmod.com) and [Tetra](https://github.com/17cupsofcoffee/tetra) to finally step away from the awfulness that the C#/.NET platform is in my opinion. This project fixes:
- [bad-visualizer](https://gitlab.com/fililip/bad-visualizer), the same thing as above but with the additional benefits of:
  - terrible performance
  - MonoGame (no link, I don't wanna remember that framework ever again)
  - being written in C# and thus not allowing for *proper* AOT
  - much more...
- [rectangler](https://gitlab.com/fililip/rectangler), "a tool that CAN be used to select bounding boxes of objects in images for cnn based object detectors" which was later used by me during my YOLOv4/Darknet/OpenCV phase
- the rest you can pretty much find on my profile here

© 6483 labs, 2018-2023
